# Filename: utorrent.py
# Purpose: add files to Deluged with Taiga by posing as uTorrent
# Project Name: Paiga
# Author: Devian50
from subprocess import call
import argparse
import ntpath #Purely for Debugging messages
import sys
import ctypes
import shlex
debug=True #DON'T FORGET TO SET THIS TO FALSE BEFORE USING THIS SCRIPT
#Test Edit
dServerUsername='Devian' #Deluge Server Username
dServerPassword='8536molecule6358' #Deluge Server Password (Remote password, not WebUI password)
dServerAddress='localhost'	#IP Address or domain name of the Deluge server
dServerPort='58846' #Port of the Deluge Server

def addTorrent(directory, files):
	delugeConAddCommand=['deluge-console.exe', 'add', '--path='+directory]
	delugeConAddCommand+=files
	if not debug:
		retCode=call(delugeConAddCommand)
		if retCode==0:
			print "Adding Torrents: Succeeded"
		else:
			print "Adding Torrents: Failed"
	else:
		print "\b, not adding files.\nFiles that would be added are as follows:"
		for file in files:
			print ntpath.basename(file)[:-1]
		print "These files would be downloaded to:\n"+directory
		retCode=0
	if retCode==1:
		exit(retCode)

def connectServer(User, Pass, IP, Port):
	delugeConConnectCommand=['deluge-console.exe', 'connect', IP+':'+Port, User, Pass]
	if not debug:
		retCode=call(delugeConConnectCommand)
		if retCode==0:
			print "Connection: Succeeded"
		else:
			print "Connection: Failed.\nPlease ensure the server is running."
	else:
		print "Debug enabled: not connecting",
		retCode=0
	if retCode==1:
		exit(retCode)
				
def getRawArgv():
	
	func = ctypes.windll.kernel32.GetCommandLineA
	func.restype = ctypes.c_char_p
	cmd = ctypes.windll.kernel32.GetCommandLineA()
	return cmd
	
def main(argvs):
	parser = argparse.ArgumentParser(description='Adds torrents to Deluge.', prefix_chars='-+/')
	parser.add_argument('/directory', action='store')
	parser.add_argument('file', metavar='<torrent|magnet|infohash>', nargs='+', action='store')
	args=parser.parse_args(argvs[1:])
	if debug:
		print "args= "+str(args)
	directory='"'+args.directory+'"'
	if debug:
		print "directory= "+directory
	files=args.file
	files=['"'+file+'"' for file in files]
	if debug:
		for x in range(len(files)):
			print "file "+str(x)+"= "+str(files[x])
	
	connectServer(dServerUsername, dServerPassword, dServerAddress, dServerPort)
	addTorrent(directory, files)
	
if __name__=="__main__":
	print getRawArgv()
	try:
		print 'starting main and passing args'
		main(shlex.split(getRawArgv()))
	except:
		sys.exc_info()
	if not debug:
		call("timeout /t 10")
	else:
		call("timeout /t -1")